import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StyleSheet, Text, View, FlatList, TextInput, Button} from 'react-native';

export default function App() {
const [tasks,settasks] = React.useState([{task: 'New task', key: '1'}]);

function TaskItem({item, pressHandler}) {
  return (
    <View>
    <Text style = {Styles.task}>{item.task}</Text>
    <Button title = '✓'
     onPress = {() => pressHandler(item.key)}/></View>)};

function AddTask({submit}) {
const [text, setText] = React.useState('');
const change = (val) => {setText(val);}
    return (
      <View style = {Styles.content}>
      <TextInput style = {Styles.textbox}
        placeholder = 'Type here..'
        onChangeText = {change}/>
      <Button title ='Add task'
        onPress = {() => submit(text)}/></View>)};

const submit = (text) => {
  settasks((prevtasks) => {
    return [
      {task: text, key: Math.random().toString()},
       ...prevtasks]});}
        
const pressHandler = (key) => {
  settasks((prevtasks) => {
    return prevtasks.filter(slime => slime.key != key);});}

const slime = createNativeStackNavigator();

function TodoScreen ({navigation}) {
    return (
     <View style = {Styles.container}>
     <AddTask submit = {submit}/>
     <FlatList
      data = {tasks}
      renderItem = {({item}) => (
     <TaskItem item = {item} pressHandler = {pressHandler}/>)}/>
     <View>
     <Button title = "COMPLETED TASKS GO HERE"
      onPress ={() => navigation.navigate("Completed Tasks")}/>
     </View>
     </View>)};

function ComTaScreen () {
    return (<View></View>);}

  return (
    <NavigationContainer>
    <slime.Navigator initialRouteName = "TODO">
    <slime.Screen name = "TODO" component = {TodoScreen}
     options = {{headerStyle: {backgroundColor: 'yellow'}}}/>
    <slime.Screen name = "Completed Tasks" component = {ComTaScreen}
     options = {{headerStyle: {backgroundColor: 'yellow'}}}/>
    </slime.Navigator>
    </NavigationContainer>);}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'},
  task: {
    textAlign: 'center',
    padding: 15,
    marginTop: 15,
    borderColor: 'gray',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 10},
    textbox: {
      borderColor: 'black',
      borderWidth: 2,
      backgroundColor: 'skyblue',
      textAlign: 'center',
      marginTop: 25},
    content: {
      padding: 40}});
