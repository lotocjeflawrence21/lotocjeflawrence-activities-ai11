import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions} from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

const slime = createNativeStackNavigator();

const {width, height} = Dimensions.get("screen")

function Screen1({navigation}) {
    return (
        <View style = {Styles.container}>
        <Image source = {require ('./screenshots/Dream_world_slide1.png')} style = {{height: '75%', width, resizeMode: 'contain'}}/>
        <Text style = {Styles.Title}>Why chose IT?</Text>
        <Text style = {Styles.Subtitle}>I want to create my own game in the future</Text>
        <View style = {{flexDirection: 'row'}}>
        <TouchableOpacity style = {Styles.btn} onPress ={() => navigation.navigate("screen2")}>
        <Text style = {{fontWeight: 'bold', fontSize: 15}}>CONTINUE</Text>
        </TouchableOpacity>
        </View>
        </View>)};
function Screen2({navigation}) {
    return (
        <View style = {Styles.container}>
        <Image source = {require ('./screenshots/Inspiration_slide2.png')} style = {{height: '75%', width, resizeMode: 'contain'}}/>
        <Text style = {Styles.Title}>Why chose IT?</Text>
        <Text style = {Styles.Subtitle}>I was inspired by the creators of my favorite games like Stardew Valley and Minecraft</Text>
        <View style = {{flexDirection: 'row'}}>
        <TouchableOpacity style = {Styles.btn} onPress ={() => navigation.navigate("screen3")}>
        <Text style = {{fontWeight: 'bold', fontSize: 15}}>CONTINUE</Text>
        </TouchableOpacity>
        </View>
        </View>)};
function Screen3({navigation}) {
    return (
        <View style = {Styles.container}>
        <Image source = {require ('./screenshots/Knowledge_slide3.png')} style = {{height: '75%', width, resizeMode: 'contain'}}/>
        <Text style = {Styles.Title}>Why chose IT?</Text>
        <Text style = {Styles.Subtitle}>I want to increase my knowledge about computers</Text>
        <View style = {{flexDirection: 'row'}}>
        <TouchableOpacity style = {Styles.btn} onPress ={() => navigation.navigate("screen4")}>
        <Text style = {{fontWeight: 'bold', fontSize: 15}}>CONTINUE</Text>
        </TouchableOpacity>
        </View>
        </View>)};
function Screen4({navigation}) {
    return (
        <View style = {Styles.container}>
        <Image source = {require ('./screenshots/MobileDevelop_slide4.png')} style = {{height: '75%', width, resizeMode: 'contain'}}/>
        <Text style = {Styles.Title}>Why chose IT?</Text>
        <Text style = {Styles.Subtitle}>I want to experience and know how applications are made</Text>
        <View style = {{flexDirection: 'row'}}>
        <TouchableOpacity style = {Styles.btn} onPress ={() => navigation.navigate("screen5")}>
        <Text style = {{fontWeight: 'bold', fontSize: 15}}>CONTINUE</Text>
        </TouchableOpacity>
        </View>
        </View>)};
function Screen5({navigation}) {
    return (
        <View style = {Styles.container}>
        <Image source = {require ('./screenshots/Searching_slide5.png')} style = {{height: '75%', width, resizeMode: 'contain'}}/>
        <Text style = {Styles.Title}>Why chose IT?</Text>
        <Text style = {Styles.Subtitle}>I want to know what I want to do in the future by experiencing different things</Text>
        <View style = {{flexDirection: 'row'}}>
        <TouchableOpacity style = {Styles.btn} onPress ={() => navigation.navigate("screen1")}>
        <Text style = {{fontWeight: 'bold', fontSize: 15}}>HOME</Text>
        </TouchableOpacity>
        </View>
        </View>)};

export default function App() {
    return (
      <NavigationContainer>
      <slime.Navigator screenOptions = {{headerShown: false}} initialRouteName = "screen1">
      <slime.Screen name = "screen1" component = {Screen1}/>
      <slime.Screen name = "screen2" component = {Screen2}/>
      <slime.Screen name = "screen3" component = {Screen3}/>
      <slime.Screen name = "screen4" component = {Screen4}/>
      <slime.Screen name = "screen5" component = {Screen5}/>
      </slime.Navigator>
      </NavigationContainer>)};

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'yellow',
        alignItems: 'center'},
    Title: {
        color: 'darkblue',
        fontSize: 23,
        fontWeight: 'bold',
        marginTop: 10,
        textAlign: 'center'},
    Subtitle: {
        color: 'green',
        fontSize: 15,
        marginTop: 10,
        maxWidth: '60%',
        textAlign: 'center',
        lineHeight: 15},
    btn: {
        flex: 1,
        borderWidth: 2,
        height: 45,
        borderRadius: 10,
        backgroundColor: 'skyblue',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20}});