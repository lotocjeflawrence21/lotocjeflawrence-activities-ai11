import React from 'react';
import {Text, View, TextInput, Alert, StyleSheet, Button } from 'react-native';
export default function App() {
const [text, setText] = React.useState('')
  return (
    <View style={Styles.container}>
    <TextInput onChangeText = {(text) => setText(text)}
    style = {Styles.input}
    placeholder = 'Type here..'/>
    <Button title = 'Ok'
     onPress = {() => {alert(text)}}/>
    </View>);}

const Styles = StyleSheet.create({
  container:
     {flex: 1,
      justifyContent: 'center',
      alignItems: 'center'},
  input:
     {borderColor:'black',
      borderWidth: 2,
      width: 100}});


  
