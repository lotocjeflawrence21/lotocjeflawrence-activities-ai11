import { StyleSheet, Text, View, Button, TouchableOpacity, FlatList } from 'react-native';
import { Component } from 'react/cjs/react.production.min';
export default class App extends Component {
  constructor() {
    super()
    this.state = {resultext : "",
                  calcutext: ""}
    this.operations = ['Del','+','-','*','/']}

calcuresult() {
  const text = this.state.resultext
  this.setState({
      calcutext: eval(text)})}

validate() {
    const text = this.state.resultext
    switch(text.slice(-1)) {
        case '+':
        case '-':
        case '*':
        case '/':
            return false}
            return true}

btnPressed(text) {

    if(text == '=') {return this.validate() && this.calcuresult()}

    this.setState({resultext: this.state.resultext + text})}

operate(operations) {
    switch(operations) {
        case 'Del':
            let text = this.state.resultext.split('')
            text.pop()
            this.setState({resultext: text.join('')})
            break
        case '+':
        case '-':
        case '*':
        case '/':
            const lastchar = this.state.resultext.split('').pop()

            if(this.operations.indexOf(lastchar) > 0) return

            if (this.state.text == "") return
            this.setState({resultext: this.state.resultext + operations})}}

render() {
            const text = this.state.resultext
            const text2 = this.state.calcutext
  let rows = []
  let nums = [[1,2,3], [4,5,6], [7,8,9], ['.',0,'=']]
  for(let i = 0; i < 4; i++) {
      let row = []
      for(let j = 0; j < 3; j++) {
          row.push(<TouchableOpacity key = {nums[i][j]} onPress = {() => this.btnPressed(nums[i][j])}style = {Styles.btn}>
                    <Text style = {Styles.btntext}>{nums[i][j]}</Text>
                   </TouchableOpacity>)}
      rows.push(<View key = {i} style = {Styles.row}>{row}</View>)}

  let ops = []
  for(let i = 0; i < 5; i++) {
      ops.push(<TouchableOpacity key = {this.operations[i]}onPress = {() => this.operate(this.operations[i])}style = {Styles.btn}>
                 <Text style = {[Styles.btntext, Styles.operbtn]}>{this.operations[i]}</Text>
               </TouchableOpacity>)}
   
  return (
    <View style = {Styles.container}>
        <View style = {Styles.history}>
          <Text style = {Styles.histotext}>{text}={text2}</Text>
        </View>
        <View style = {Styles.result}>
            <Text style = {Styles.resultext}>{this.state.resultext}</Text>
        </View>
        <View style = {Styles.calculation}>
            <Text style = {Styles.calcutext}>{this.state.calcutext}</Text>
        </View>
        <View style = {Styles.buttons}>
            <View style = {Styles.numbers}>{rows}</View>
            <View style = {Styles.operations}>{ops}</View>
        </View>
    </View>);}}



const Styles = StyleSheet.create({
  container: {
    flex: 1},
  result: {
    flex: 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'flex-end'},
  calculation: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'flex-end'},
  buttons: {
    flex: 7,
    flexDirection: 'row'},
  numbers: {
    flex: 3,
    backgroundColor: '#434343'},
  operations: {
    flex: 1,
    backgroundColor: '#636363',
    justifyContent: 'space-around',
    alignItems: 'stretch'},
  row: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center'},
  calcutext: {
    fontSize: 24,
    color: 'blue'},
  resultext: {
    fontSize: 30,
    color: 'green'},
  btn: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center'},
  btntext: {
    fontSize: 30,
    color: 'white'},
  operbtn: {
    color: 'white'},
  history: {
    color: 'blue',
    backgroundColor: 'lightgreen',
    flex: 1,
    padding: 30,
    color: 'black'},
  histotext: {
    fontSize: 20,
    padding: 20}});