import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, FlatList, TextInput, Modal, Linking, Platform, Image } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { MaterialIcons } from '@expo/vector-icons';

export default function App() {

  const [contact, setContact] = useState([{name: 'John Doe', phonenum: '09123456789', key: 1}]);
  const [modalVisible, setmodalVisible] = useState(false);


  const Slime = createNativeStackNavigator();


  //function for the Items
  function ContactItem ({item}) {

    const [text, setText] = React.useState('');
    const [number, setnumber] = React.useState('');
    const changeName = (val) => {setText(val);}
    const changeNumber = (val) => {setnumber(val);}

    //Variable for the editing an Item
    const editPress = (key) => {
      setmodalVisible(true);
      const NewContact = contact.filter((contact) => contact.key != key);
      setContact(NewContact);}
    
    //Variable for the save button to update an Item
    const updateItem = (text, number) => {
      setContact((currentContact) => { 
        return [{name: text, phonenum: number, key: Math.random()}, ...currentContact]});
      setmodalVisible(false);}

    //Variable for deleting an Item
    const deleter = (key) => {
      const NewContact = contact.filter((contact) => contact.key != key);
      setContact(NewContact);}

    //Variable for calling an Item
    const call = (phonenum) => { 
      let phonenumber = {phonenum};

      if(Platform.OS == 'android') {phonenumber = `tel:${phonenum}`;}
      else {phonenumber = `telprompt:${phonenum}`;}
      Linking.openURL(phonenumber);}

    return (
      <View style = {Style.list}>

          <View style = {{flexDirection: 'row', alignItems: 'center'}}>
            <Image source = {require ('./Avatar/AvatarIcon1.png')} style = {Style.ImageIcon}/>
          <View style = {{paddingLeft: 10}}>
            <Text style = {{fontSize: 18, fontWeight: 'bold'}}>{item.name}</Text>
            <Text>{item.phonenum}</Text>
          </View>
          </View>

        <TouchableOpacity onPress = {() => deleter(item.key)}>
          <MaterialIcons name = 'delete' size = {25} color = 'blue'/>
        </TouchableOpacity>
        <TouchableOpacity onPress = {() => editPress(item.key)}>
          <MaterialIcons name = 'edit' size = {25} color = 'green'/>
        </TouchableOpacity>
        <TouchableOpacity onPress = {() => call(item.phonenum)}>
          <MaterialIcons name = 'call' size = {25} color = 'red'/>
        </TouchableOpacity>

        <Modal animationType = 'fade' visible = {modalVisible} onRequestClose = {() => setmodalVisible(false)}>
          <View style = {Style.container}>
            <Text style = {Style.title}>EDIT CONTACT</Text>
            <Image source = {require ('./Avatar/AvatarIcon1.png')} style = {Style.ImageIcon2}/>
          <TextInput style = {Style.textbox}
            placeholder = "New Name"
            onChangeText = {changeName}
            maxLength = {20}/>
          <TextInput style = {Style.textbox} 
            placeholder = "New Phone Number"
            onChangeText = {changeNumber}
            keyboardType = "numeric"
            maxLength = {11}/>

          </View>
            <View style = {{marginLeft: 45, marginRight: 45}}>
              <TouchableOpacity onPress = {() => updateItem(text, number)}>
                <Text style = {Style.textButton}>SAVE</Text>
              </TouchableOpacity>
            </View>
        </Modal>
      </View>);}


  //Function for the AddContact Screen
  function AddContact ({navigation}) {

    const [text, setText] = React.useState('');
    const [number, setnumber] = React.useState('');
    const changeName = (val) => {setText(val);}
    const changeNumber = (val) => {setnumber(val);}

    //Variable for adding the Item in Contact List
    const submit = (text, number) => {
      navigation.navigate("CONTACT");
      setContact((contact) => { 
        return [{name: text, phonenum: number, key: Math.random()},...contact]});}

    return (
      <View style = {Style.container}>
        <Text style = {Style.title}>NEW CONTACT</Text>
        <Image source = {require ('./Avatar/AvatarIcon1.png')} style = {Style.ImageIcon2}/>
        <TextInput style = {Style.textbox}
          placeholder = "Name"
          onChangeText = {changeName}
          maxLength = {20}/>
        <TextInput style = {Style.textbox} 
          placeholder = "Phone Number"
          onChangeText = {changeNumber}
          keyboardType = "numeric"
          maxLength = {11}/>
      
        <View style = {{paddingLeft: 85, flexDirection: 'row'}}>
        <TouchableOpacity onPress = {() => submit(text, number)}>
          <Text style = {Style.textButton}>SAVE</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress = {() => navigation.navigate("CONTACT")}>
          <Text style = {Style.textButton}>BACK</Text>
        </TouchableOpacity>
        </View>
      </View>);}


  //Variable for the ContactList Screen
  function ContactList({navigation}) {
    return (
      <View style = {Style.container}>
        <Text style = {Style.title}>MY CONTACTS</Text>
          <TouchableOpacity onPress = {() => navigation.navigate("ADD CONTACT")}>
            <Text style = {Style.textButton}>ADD NEW CONTACT</Text>
          </TouchableOpacity>

        <View style = {{marginTop: 20}}>
          <FlatList
            data = {contact}
            renderItem = {({item}) => (<ContactItem item = {item}/>)}/>
        </View>

      </View>);}

  return (
    <NavigationContainer>
      <Slime.Navigator screenOptions = {{headerShown: false}} initialRouteName = "CONTACT">
        <Slime.Screen name = "CONTACT" component = {ContactList}/>
        <Slime.Screen name = "ADD CONTACT" component = {AddContact}/>
      </Slime.Navigator>
    </NavigationContainer>);}


const Style = StyleSheet.create({
    container: {
      padding: 30,
      paddingTop: 80},
    title: {
      textAlign: "center",
      fontSize: 20,
      fontWeight: "bold",
      marginBottom: 25},
    textbox: {
      borderWidth: 2,
      borderColor: 'blue',
      height: 50,
      marginBottom: 20,
      borderRadius: 10,
      padding: 15},
    textButton: {
      textAlign: "center",
      borderWidth: 2,
      borderColor: "gray",
      borderRadius: 10,
      backgroundColor: "yellowgreen",
      padding: 15,
      margin: 3},
    list: {
      alignItems: 'center',
      borderWidth: 3,
      borderColor: 'skyblue',
      backgroundColor: 'white',
      height: 80,
      marginBottom: 10,
      borderRadius: 10,
      padding: 10,
      flexDirection: 'row',
      justifyContent: 'space-between'},
    ImageIcon: {
      resizeMode: 'contain', 
      height: 70, 
      width: 50},
    ImageIcon2: {
      resizeMode: 'contain', 
      height: 120, 
      width: 100, 
      marginBottom: 20, 
      marginLeft: 120}});
    
